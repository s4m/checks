#!/bin/bash

error=0

echo '=============== rubocop ================'
if ! rubocop; then
  ((error++))
fi

echo '============== foodcritic =============='
if ! foodcritic -f any .; then
  ((error++))
fi

echo '============= lines length ============='
if ! curl -s https://gitlab.com/s4m/checks/raw/master/check_lines_length.sh | \
  bash -s -- 80 '\./\.kitchen'; then
  ((error++))
fi

echo '============= git_history =============='
if ! curl -s https://gitlab.com/s4m/checks/raw/master/check_git_history.sh | \
  bash; then
  ((error++))
fi

echo '================= misc ================='
if ! grep skip_preparation .kitchen.yml > /dev/null; then
  echo '==> Add skip_preparation!'
  ((error++))
fi

if ! grep 'Template chef 20170202' .gitlab-ci.yml > /dev/null; then
  echo '==> Use latest template for .gitlab-ci.yml';
  ((error++))
fi

if [ -e 'test/kitchen_command.rb' ]; then
  if ! grep 'rescue StandardError' 'test/kitchen_command.rb' > /dev/null; then
    echo '==> Fix cleaning in test/kitchen_command.rb, look at imply'
    ((error++))
  fi
fi

if ! grep 'always_update_cookbooks' .kitchen.yml > /dev/null; then
  echo '==> Set always_update_cookbooks: true'
  ((error++))
fi

if git branch -a | grep develop > /dev/null; then
  echo '==> Please remove branch develop'
  ((error++))
fi

if ! grep -r cookbook_name attributes > /dev/null; then
  echo '==> sed cookbook_name everywhere'
  ((error++))
fi

if ! find . -type f -print0 | \
  xargs -0 grep 'Copyright (c) .*2017 Sam4Mobile' > /dev/null; then
  echo '==> Change copyright to 2017!'
  ((error++))
fi

echo '================= end =================='
echo "DO NOT FORGET the git clean -fxd"

exit $error
